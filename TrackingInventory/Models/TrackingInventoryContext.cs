﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TrackingInventory.Models
{
    public class TrackingInventoryContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public TrackingInventoryContext() : base("name=TrackingInventoryContext")
        {
            //  ContextOptions.ProxyCreationEnabled = false;
          //  Configuration.ProxyCreationEnabled = false;
            //DbContext.ProxyCreationEnabled = false;
        }

        public System.Data.Entity.DbSet<TrackingInventory.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<TrackingInventory.Models.InventoryItem> InventoryItems { get; set; }

        public System.Data.Entity.DbSet<TrackingInventory.Models.Role> Roles { get; set; }

      //  DbContext.Configuration.ProxyCreationEnabled = false;

    }
}
