﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackingInventory.Models
{
  public  class InventoryItem
    {
       [Key]
        public int InventoryID { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        //foreign Key
     //   [ForeignKey("UserID")]
        //public int UserID { get; set; }
        //public virtual User user { get; set; }
    }
}
