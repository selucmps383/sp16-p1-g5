namespace TrackingInventory.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Helpers;
    internal sealed class Configuration : DbMigrationsConfiguration<TrackingInventory.Models.TrackingInventoryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TrackingInventory.Models.TrackingInventoryContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

   

            //    context.Users.AddOrUpdate(
            //        u => u.FirstName,
            //        new User { UserID = 1, FirstName = "383Admin", LastName = "383Admin", UserName = "Admin", Password = Crypto.HashPassword("selu2014"), Role = 01 },
            //         new User { UserID = 2, FirstName = "Pankaj", LastName = "Sherchan", UserName = "pankajsherchan1995", Password = Crypto.HashPassword("pankaj"), Role = 02 }
            //        );


            context.InventoryItems.AddOrUpdate(
                u => u.Name,
                new InventoryItem { InventoryID = 2, Name = "Volleyball", Quantity = 05}); /*user = new User { UserID = 2, FirstName = "Pankaj", LastName = "Sherchan", UserName = "pankajsherchan1995", Password = Crypto.HashPassword("pankaj"), Role = 02 } });*/

            //   context.Roles.AddOrUpdate(

            //        r => r.RoleName,
            //        new Role { RoleId = 01, RoleName = "Admin" },
            //        new Role { RoleId = 02, RoleName = "User" }

            //        );
            }
        }
    }


