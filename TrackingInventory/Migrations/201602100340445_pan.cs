namespace TrackingInventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pan : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InventoryItems", "UserID", "dbo.Users");
            DropIndex("dbo.InventoryItems", new[] { "UserID" });
            DropColumn("dbo.InventoryItems", "UserID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InventoryItems", "UserID", c => c.Int(nullable: false));
            CreateIndex("dbo.InventoryItems", "UserID");
            AddForeignKey("dbo.InventoryItems", "UserID", "dbo.Users", "UserID", cascadeDelete: true);
        }
    }
}
