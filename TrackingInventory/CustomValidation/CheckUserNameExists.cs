﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackingInventory.Models;

namespace TrackingInventory.CustomValidation
{
    public class CheckUserNameExists : ValidationAttribute
    { 
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value == null)
            {
                return new ValidationResult("Model is Empty");
            }

            TrackingInventoryContext db = new TrackingInventoryContext();
            var user = db.Users.FirstOrDefault(u => u.UserName == (string)value );
            if (user == null)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Username Already Exists");
            }
        }


    }
}