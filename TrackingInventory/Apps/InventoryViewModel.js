﻿
var InventoryViewModel = function () {
    var self = this;
    self.inventories = ko.observableArray();
    
    self.watchModel = function (model, callback) {
        for (var key in model) {
            if (model.hasOwnProperty(key) && ko.isObservable(model[key])) {
                self.subscribeToProperty(model, key, function (key, val) {
                    callback(model, key, val);

                });

            }

        }
    }

    self.subscribeToProperty = function (model, key, callback) {

        model[key].subscribe(function (val) {
            callback(key, val);


        });
    }

    self.modelChanged = function (model, key, val) {
        //alert(model.InventoryID);
        //alert(key);
        //alert(val);
        var payload = {};
        payload[key] = val;
        $.ajax({

            url: '/odata/InventoryItems(' + model.InventoryID + ')',
            type: 'PATCH',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            dataType: 'json'

        });
    }



    $.getJSON('/odata/InventoryItems', function (data) {
        self.inventories(ko.utils.arrayMap(data.value, function (inventory) {

            var obsInventory = {
                InventoryID: inventory.InventoryID,
                Name: ko.observable(inventory.Name),
                Quantity: ko.observable(inventory.Quantity)
            }
            self.watchModel(obsInventory, self.modelChanged);
            return obsInventory;
        }));
        
     
    });
}



$(function () {
    var pankaj = $.connection.inventory;
    var viewModel = new InventoryViewModel();

    var findInventory = function (id) {

        return ko.utils.arrayFirst(viewModel.inventories(), function (item) {

            if (item.Id == id) {

                return item;
            }
        });
    }

  
   
    inventorySignalR.client.updateInventory = function (id, key, value){
        var inventory = findInventory(id);
        inventory[key](value);

    }

    ko.applyBindings(viewModel);
    $.connection.hub.start();
    

   
});



   