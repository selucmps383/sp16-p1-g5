﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using TrackingInventory.Filters;
using TrackingInventory.Hubs;
using TrackingInventory.Models;

namespace TrackingInventory.Controllers

{
   
        //public class InventoryItemsController : ApiController
        public class InventoryItemsController : EntitySetControllerWithHub<InventoryHub> { 
       // ODataController
        
    
        private TrackingInventoryContext db = new TrackingInventoryContext();

        [EnableQuery]
        public override IQueryable<InventoryItem> Get()
        {

        
            return db.InventoryItems;
        }

        protected override InventoryItem GetEntityByKey(int key) {
            return db.InventoryItems.Find(key);

        }

        protected override InventoryItem PatchEntity(int key, Delta<InventoryItem> patch) {

            var inventoryToPatch = GetEntityByKey(key);
            patch.Patch(inventoryToPatch);
            db.Entry(inventoryToPatch).State = EntityState.Modified;
            db.SaveChanges();

            var changedProperty = patch.GetChangedPropertyNames().ToList()[0];

            object changedPropertyValue;
            patch.TryGetPropertyValue(changedProperty, out changedPropertyValue);
            

            Hub.Clients.All.updateInventory(inventoryToPatch.InventoryID, changedProperty, changedPropertyValue);
            return inventoryToPatch;





        }

        
        // POST: api/Employees
     
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.InventoryItems.Count(e => e.InventoryID == id) > 0;
        }
    }
}

























