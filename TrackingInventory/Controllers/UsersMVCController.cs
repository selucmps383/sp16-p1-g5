﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using TrackingInventory.Models;
using TrackingInventory.CustomValidation;

namespace TrackingInventory.Controllers
{
    public class UsersMVCController : Controller
    {
        private TrackingInventoryContext db = new TrackingInventoryContext();

        // GET: UsersMVC
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: UsersMVC/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: UsersMVC/Create
       
        public ActionResult Create()
        {
            //ViewBag.RoleID = new SelectList(db.Roles, "RoleId", "RoleName");
            return View();
        }

        // POST: UsersMVC/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,UserName,FirstName,LastName,Password")] User user)
        {
            if (ModelState.IsValid)
            {
             
                var hashedPassword = Crypto.HashPassword(user.Password);
                user.Password = hashedPassword;
                user.Role = 02;
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.RoleID = new SelectList(db.Roles, "RoleId", "RoleName", user.RoleId);
            return View(user);
        }


      
        public ActionResult SignIn()
        {
            return View();
        }


        [HttpPost]
        public ActionResult SignIn(SignInViewModel LogingUser)
        {
            var checkUserIfExists = db.Users.FirstOrDefault(u => u.UserName.Equals(LogingUser.UserName));
        
            
            if (ModelState.IsValid && checkUserIfExists != null)
            {
                bool verifyPassword = Crypto.VerifyHashedPassword(checkUserIfExists.Password, LogingUser.Password);
                if (verifyPassword == true)
                {

                    // creating authetication ticket
                    FormsAuthentication.SetAuthCookie(LogingUser.UserName, false);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    @ViewBag.Message = "Error.Ivalid login.";
                    return RedirectToAction("SignInFailure", "FundRaisers");
                }

            }

            ModelState.AddModelError("UserDoesNotExist", "Username or Password is Incorrect! Please try Again!!");
            return View(LogingUser);
        }


     
        [HttpPost]
        public JsonResult CheckForDuplication(string UserName)
        {
            var data = db.Users.Where(p => p.UserName.Equals(UserName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            if (data != null)
            {
                return Json("Sorry, this name already exists", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: UsersMVC/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
      
        [HttpPost]
      
        public ActionResult Edit([Bind(Include = "UserID,UserName,FristName,LastName,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: UsersMVC/Delete/5

        [CustomUserAuthorize(Permission ="User")]
        public ActionResult Test()
        {
            return View();
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: UsersMVC/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult Logout()
        {
            // This is the predefined SignOut method in FormAuthentication :p
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");

        }
    }
}
