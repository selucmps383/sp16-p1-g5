﻿var InventoryViewModel = function () {

    var self = this;
    self.inventories = ko.observableArray();

    self.displayMode = function (inventory) {

       return  inventory.Edit() ? 'edit-template' : 'read-template';
    }

    self.remove = function () {
       self.sendDelete(self);
    };

    self.purchaseItem = function (model, key, val) {
        var payload = {};
        payload["Quantity"] = model.Quantity() - 1 > 0 ?  model.Quantity() - 1 : 0;
        $.ajax({

            url: '/odata/InventoryItems(' + model.Id + ')',
            type: 'PATCH',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            dataType: 'json'

        });

    };

   

    self.sendDelete = function (item) {
        $.ajax({
            url: '/odata/InventoryItems/' + item.Id,
            type: 'DELETE'
        });
    }




    self.remove = function (inventory) {
        $.ajax({
            url: '/odata/InventoryItems(' + inventory.Id + ')',
            type: 'DELETE',
            contentType: 'application/json',
            dataType: 'json'
        });
    }

    self.edit = function (inventory) {

        inventory.Edit(true);
    }

    self.done = function (inventory) {

        inventory.Edit(false);
    }


    //self.addItemTitle = ko.observable("");
    //self.items = ko.observableArray();

    //self.add = function (id, title, finished) {
    //    self.items.push(new ToDoItem(self, id, title, finished));
    //};

    self.add = function () {
        var payload = { Name: "New", Quantity: 1, Price: 1, UserID: 2 };

        $.ajax({
            url: '/odata/InventoryItems',
            type: 'POST',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            dataType:'json'
            
            


        });
    }

    self.add = function () {
        var payload = { Name: "New", Quantity: 1, Price: 1, UserID : 2 };
        $.ajax({
            url: '/odata/InventoryItems',
            type: 'POST',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            dataType: 'json'
        });
    }


    self.watchModel = function (model, callback) {
        for (var key in model) {
            if (model.hasOwnProperty(key) && ko.isObservable(model[key])) {
                self.subscribeToProperty(model, key, function (key, val) {
                    callback(model, key, val);

                });

            }

        }
    }

    self.subscribeToProperty = function (model, key, callback) {

        model[key].subscribe(function (val) {
            callback(key, val);


        });
    }

    self.modelChanged = function (model, key, val) {
        //alert(model.InventoryID);
        // alert(key);
        //  alert(val);
        var payload = {};
        payload[key] = val;
        $.ajax({

            url: '/odata/InventoryItems(' + model.Id + ')',
            type: 'PATCH',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            dataType: 'json'

        });
    }

   

    $.getJSON('odata/InventoryItems', function (data) {

        self.inventories(ko.utils.arrayMap(data.value, function (inventory) {
            var obsInventory = {
                Id: inventory.Id,
                Name: ko.observable(inventory.Name),
                Quantity: ko.observable(inventory.Quantity),
                Price: ko.observable(inventory.Price),
                UserID: inventory.UserID,
                Edit: ko.observable(false)

            }
            self.watchModel(obsInventory, self.modelChanged)

            return obsInventory;
        }));

       
    });

}




$(function () {

    var hub = $.connection.inventory;
    var viewModel = new InventoryViewModel();

    var findInventory = function (id) {

        return ko.utils.arrayFirst(viewModel.inventories(), function (item) {

            if (item.Id == id) {

                return item;
            }
        });
    }

    //hub.addItem = function (item) {
    //    viewModel.add(item.ID, item.Title, item.Finished);
    //};

    hub.client.addInventory = function (inventory) {
        var obsInventory = {
            Id: inventory.Id,
            Name: ko.observable(inventory.Name),
            Quantity: ko.observable(inventory.Quantity),
            Price: ko.observable(inventory.Price),
           // Edit: ko.observable(false),
            //Locked: ko.observable(employee.Locked)
        }
        viewModel.inventories.push(obsInventory);
        viewModel.watchModel(obsInventory, viewModel.modelChanged);
    }

    //pankaj.removeEmployee = function (id) {
    //    viewModel.remove(id);
    //};

    hub.client.removeInventory = function (id) {
        viewModel.inventories.remove(function (item) { return item.Id == id });
    }

    hub.client.updateInventory = function (id, key, value) {
        var inventory = findInventory(id);
        inventory[key](value);

    }

    ko.applyBindings(viewModel);
    $.connection.hub.start();



});




