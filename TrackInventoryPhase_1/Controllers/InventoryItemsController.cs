﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using TrackInventoryPhase_1.Hubs;
using TrackInventoryPhase_1.Models;
using System.Web.Http.OData.Results;
using TrackInventoryPhase_1.CustomValidation;

namespace TrackInventoryPhase_1.Controllers
{
    public class InventoryItemsController : EntitySetControllerWithHub<InventoryHub>
    {
        private TrackInventoryPhase_1Context db = new TrackInventoryPhase_1Context();




        public override IQueryable<InventoryItem> Get()
        {

            return db.InventoryItems;
        }

        protected override InventoryItem GetEntityByKey(int key)
        {
            return db.InventoryItems.Find(key);
        }


        [System.Web.Http.Authorize]
        protected override InventoryItem PatchEntity(int key, Delta<InventoryItem> patch)
        {

            var inventoryToPatch = GetEntityByKey(key);
            patch.Patch(inventoryToPatch);
            db.Entry(inventoryToPatch).State = EntityState.Modified;
            db.SaveChanges();
            var changedProperty = patch.GetChangedPropertyNames().ToList()[0];
            object changedPropertyValue;
            patch.TryGetPropertyValue(changedProperty, out changedPropertyValue);
            Hub.Clients.All.updateInventory(inventoryToPatch.Id, changedProperty, changedPropertyValue);
            return inventoryToPatch;
        }


        [HttpPost]
        protected override InventoryItem CreateEntity(InventoryItem entity)
        {
            var newInventory = db.InventoryItems.Add(entity);
            db.SaveChanges();

            Hub.Clients.All.addInventory(newInventory);
            return newInventory;
        }

        [HttpPost]
        //public void PurchaseItem(int itemId)
        //{
        //    db.InventoryItems.Find(itemId).Quantity = db.InventoryItems.Find(itemId).Quantity - 1;
        //    db.SaveChanges();
        //    Hub.Clients.All.UpdateInventory(itemId, { )
        //}




        //   [CustomUserAuthorize(Permission = "Admin")]
        [System.Web.Http.Authorize]
        
        public override void Delete(int key)
        {
            var employeeToDelete = db.InventoryItems.Find(key);
            db.InventoryItems.Remove(employeeToDelete);
            db.SaveChanges();

            Hub.Clients.All.removeInventory(key);
        }


        //public HttpResponseMessage PostNewToDoItem(ToDoItem item)
        //{
        //    lock (db)
        //    {
        //        // Add item to the "database"
        //        item.ID = Interlocked.Increment(ref lastId);
        //        db.Add(item);

        //        // Notify the connected clients
        //        Hub.Clients.addItem(item);

        //        // Return the new item, inside a 201 response
        //        var response = Request.CreateResponse(HttpStatusCode.Created, item);
        //        string link = Url.Link("apiRoute", new { controller = "todo", id = item.ID });
        //        response.Headers.Location = new Uri(link);
        //        return response;
        //    }
        //}
        //public HttpResponseMessage Post([FromBody]InventoryItem entity)
        //{
        //    string name = entity.Name;
        //    HttpResponseMessage response;
        //    int Quantity = entity.Quantity;
        //    int Price = entity.Price;
        //    int UserID = entity.UserID;

        //    try {
        //        db.InventoryItems.Add(entity);

        //        response = Request.CreateResponse(HttpStatusCode.Created, entity);
        //      //  var response = Request.CreateResponse(HttpStatusCode.Created, item);
        //        string link = Url.Link("apiRoute", new { controller = "InventoryItems", id = entity.Id });
        //        response.Headers.Location = new Uri(link);
        //        return response;
        //    }
            
        //    catch (Exception)
        //    {
        //        response = Request.CreateResponse(HttpStatusCode.InternalServerError);
        //        return response;
        //    }
        //}
        //protected override InventoryItem CreateEntity(InventoryItem entity) {

        //    var newInventory = db.InventoryItems.Add(entity);
        //    db.SaveChanges();
        //    Hub.Clients.All.addInventory(newInventory);
        //    return newInventory;
        //}
     
        //// GET: api/InventoryItems
        //public IQueryable<InventoryItem> GetInventoryItems()
        //{
        //    return db.InventoryItems;
        //}

        //// GET: api/InventoryItems/5
        //[ResponseType(typeof(InventoryItem))]
        //public IHttpActionResult GetInventoryItem(int id)
        //{
        //    InventoryItem inventoryItem = db.InventoryItems.Find(id);
        //    if (inventoryItem == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(inventoryItem);
        //}

        //// PUT: api/InventoryItems/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutInventoryItem(int id, InventoryItem inventoryItem)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != inventoryItem.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(inventoryItem).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!InventoryItemExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/InventoryItems
        //[ResponseType(typeof(InventoryItem))]
        //public IHttpActionResult PostInventoryItem(InventoryItem inventoryItem)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.InventoryItems.Add(inventoryItem);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = inventoryItem.Id }, inventoryItem);
        //}

        //// DELETE: api/InventoryItems/5
        //[ResponseType(typeof(InventoryItem))]
        //public IHttpActionResult DeleteInventoryItem(int id)
        //{
        //    InventoryItem inventoryItem = db.InventoryItems.Find(id);
        //    if (inventoryItem == null)
        //    {
        //        return NotFound();
        //    }

        //    db.InventoryItems.Remove(inventoryItem);
        //    db.SaveChanges();

        //    return Ok(inventoryItem);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InventoryItemExists(int id)
        {
            return db.InventoryItems.Count(e => e.Id == id) > 0;
        }
    }
}