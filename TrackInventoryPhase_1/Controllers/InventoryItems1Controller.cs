﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TrackInventoryPhase_1.Hubs;
using TrackInventoryPhase_1.Models;

namespace TrackInventoryPhase_1.Controllers
{
    public class InventoryItems1Controller : EntitySetControllerWithHub<InventoryHub>
    {
        private TrackInventoryPhase_1Context db = new TrackInventoryPhase_1Context();

        // GET: api/InventoryItems1
        public IQueryable<InventoryItem> GetInventoryItems()
        {
            return db.InventoryItems;
        }

        // GET: api/InventoryItems1/5
        [ResponseType(typeof(InventoryItem))]
        public IHttpActionResult GetInventoryItem(int id)
        {
            InventoryItem inventoryItem = db.InventoryItems.Find(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return Ok(inventoryItem);
        }

        // PUT: api/InventoryItems1/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutInventoryItem(int id, InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventoryItem.Id)
            {
                return BadRequest();
            }

            db.Entry(inventoryItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InventoryItems1
        public HttpResponseMessage Post([FromBody]InventoryItem entity)
        {
            string name = entity.Name;
            HttpResponseMessage response;
            int Quantity = entity.Quantity;
            double Price = entity.Price;
            int UserID = entity.UserID;

            try
            {
                db.InventoryItems.Add(entity);

                response = Request.CreateResponse(HttpStatusCode.Created, entity);
                //  var response = Request.CreateResponse(HttpStatusCode.Created, item);
                string link = Url.Link("apiRoute", new { controller = "InventoryItems", id = entity.Id });
                response.Headers.Location = new Uri(link);
                return response;
            }

            catch (Exception)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                return response;
            }
        }
        // DELETE: api/InventoryItems1/5
        [ResponseType(typeof(InventoryItem))]
        public IHttpActionResult DeleteInventoryItem(int id)
        {
            InventoryItem inventoryItem = db.InventoryItems.Find(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            db.InventoryItems.Remove(inventoryItem);
            db.SaveChanges();

            return Ok(inventoryItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InventoryItemExists(int id)
        {
            return db.InventoryItems.Count(e => e.Id == id) > 0;
        }
    }
}