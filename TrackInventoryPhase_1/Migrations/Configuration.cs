namespace TrackInventoryPhase_1.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Helpers;

    internal sealed class Configuration : DbMigrationsConfiguration<TrackInventoryPhase_1.Models.TrackInventoryPhase_1Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TrackInventoryPhase_1.Models.TrackInventoryPhase_1Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            var userList = new List<Models.User>() {
              new Models.User{ FirstName = "Pao", LastName = "Yang", UserName = "Admin", Password = Crypto.HashPassword("selu2014")},
              new Models.User { FirstName = "Cara", LastName = "Tunstall",  UserName = "caratunstall", Password = Crypto.HashPassword("selu2014") },
              new Models.User { FirstName = "Andy", LastName = "Huffman", UserName = "andyhuffman", Password = Crypto.HashPassword("selu2014") }
            };
            var itemList = new List<Models.InventoryItem>() {
              new Models.InventoryItem { Name = "Socks", Price = 2D, Quantity = 200, UserID = 1 },
              new Models.InventoryItem { Name = "Shirt", Price = 10D, Quantity = 200, UserID = 1 },
              new Models.InventoryItem { Name = "Pants", Price = 2D, Quantity = 200, UserID = 1 },
              new Models.InventoryItem { Name = "Hat", Price = 2D, Quantity = 200, UserID = 1 }
              };

            foreach (var userEach in userList)
                context.Users.Add(userEach);

            context.SaveChanges();

            foreach (var itemEach in itemList)
                context.InventoryItems.Add(itemEach);

            context.SaveChanges();
        }
    }
}
