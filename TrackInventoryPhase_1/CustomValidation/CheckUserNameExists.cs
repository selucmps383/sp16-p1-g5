﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackInventoryPhase_1.Models;

namespace TrackInventoryPhase_1.CustomValidation
{
    class CheckUserNameExists : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value == null)
            {
                return new ValidationResult("Model is Empty");
            }

            TrackInventoryPhase_1Context db = new TrackInventoryPhase_1Context();

            var user = db.Users.FirstOrDefault(u => u.UserName == (string)value);
            if (user == null)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Username Already Exists");
            }
        }


    }
}