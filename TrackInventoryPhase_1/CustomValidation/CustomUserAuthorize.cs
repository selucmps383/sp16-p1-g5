﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using TrackInventoryPhase_1.Models;

namespace TrackInventoryPhase_1.CustomValidation
{

    //https://stackoverflow.com/questions/13264496/asp-net-mvc-4-custom-authorize-attribute-with-permission-codes-without-roles
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CustomUserAuthorize : AuthorizeAttribute
    {
        private TrackInventoryPhase_1Context db = new TrackInventoryPhase_1Context();

       //custom property
       public string Permission { get; set; }


        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
               return false;
            }

            string userRoleName = string.Join("", getRoleName(httpContext.User.Identity.Name.ToString())); // Call another method to get rights of the user from DB

            if (userRoleName.Contains(this.Permission))
            {
                return true;
            }
            else
            {
                return false;
            }


       }

       public string getRoleName(string UserName)     
            {
         var user = db.Users.FirstOrDefault(u => u.UserName == (UserName));

  var userRole = db.Roles.FirstOrDefault(r => user.Role == r.RoleId);

   return userRole.RoleName;

        }

       protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
           filterContext.Result = new RedirectResult("Users/SignIn");

           base.HandleUnauthorizedRequest(filterContext);
       }
    }
}
