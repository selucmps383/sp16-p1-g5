﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace TrackInventoryPhase_1.Hubs
{
    [HubName("inventory")]
    public class InventoryHub : Hub
    {
        
        public void Hello()
        {
            Clients.All.hello();
        }
    }
}